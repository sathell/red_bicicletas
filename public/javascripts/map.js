var map = L.map('main_map').setView([-0.179768, -78.486281], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?{foo}', {
    foo: 'bar', attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

/* L.marker([-0.176444, -78.485598]).addTo(map);
L.marker([-0.177206, -78.487178]).addTo(map);
L.marker([-0.179319, -78.481713]).addTo(map);
 */
$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: (result)=>{
        console.log(result);
        result.bicicletas.forEach((bici)=>{
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    }
})