const mongoose = require('mongoose')
const Bicicleta = require('../../models/bicicleta')
const Usuario = require('../../models/usuario')
const Reserva = require('../../models/reserva')


describe('Testing Usuarios', function(){
    beforeEach(function(done){
        let mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('We are connected to test database');

            done();
        })
    })

    afterEach(function(done){
        Reserva.deleteMany({}, function(err, success){
            if(err) console.log(err);
            Usuario.deleteMany({}, function(err, success){
                if(err) console.log(err)
                Bicicleta.deleteMany({}, function(err, success){
                    if(err) console.log(err);
                    done();
                })
            })
        })
    })


    describe('Cuando un Usuario reserva una bici',() => {
        it('debe existir la reserva', (done) => {
            const usuario = new Usuario({nombre: 'Javier'});
            usuario.save()
                const bicicleta = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
                bicicleta.save();
                console.log("ID __ "+bicicleta._id);
                console.log("ID .. "+bicicleta.id);
                    var hoy = new Date();
                    var mañana = new Date();
                    mañana.setDate(hoy.getDate()+1); //tambien podemos hacer add day con Moment
                    usuario.reservar(bicicleta.id, hoy, mañana, function(err, reserva){
                        Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas){
                            console.log(reservas[0]);
                            expect(reservas.length).toBe(1);
                            expect(reservas[0].diasDeReserva()).toBe(2);
                            expect(reservas[0].bicicleta.code).toBe(1);
                            expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                            /* expect(reservas[0].usuario.email).toBe(user.email);
                            expect(reservas[0].usuario.password).toBe(user.password); */
                            done();
                        });
                    });
                
            
        });
    });

})