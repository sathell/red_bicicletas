var server = require('../../bin/www');
const mongoose = require('mongoose');
const Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function(){
   /*  beforeEach(function(done) {
        const mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true});
        
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('We are connected to test database');
            done();
        });
    }); */

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, succes){
            if(err) console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstance',() => {
        it('crea una instancia de Bicicleta', (done) => {
            let bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toBe(-34.5);
            expect(bici.ubicacion[1]).toBe(-54.1);
            done();
        })
    });

    describe('Bicicleta.allBicis', () =>{
        it('comienza vacia', (done) =>{
            Bicicleta.allBicis(function(err, bicis){               
                expect(bicis.length).toBe(0);
                done();
            });
            
        });
    });   

    describe('Bicicleta.add', () => {
        it('agrega solo una bici', (done) => {
            let aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
            Bicicleta.add(aBici, function(err, newBici){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);

                    done();
                })
            })


        })
    });


    describe('BIcicleta.findByCode', () => {
        it('debe devolver la bici con code 1', (done)=>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                let aBici = new Bicicleta({code:1, color: "verde", modelo: "urbana"});
                Bicicleta.add(aBici, function(err, newBici){
                    if(err) console.log(err);
                        let aBici2 = new Bicicleta({code: 2, color: "roja", modelo: "urbana"});
                        Bicicleta.add(aBici2, function(err, newBici){
                            if(err) console.log(err);                   
        
                            Bicicleta.findByCode(2, function(error, targetBici){
                                
                                expect(targetBici.code).toBe(aBici2.code);
                                expect(targetBici.color).toBe(aBici2.color);
                                expect(targetBici.modelo).toBe(aBici2.modelo);
        
                                done();
                            })
                        })
                })
            })
        })
    })


});

/* 
beforeEach(() => { console.log('testeado')});
describe('Bicicleta.allBicis', () => {
    it('cmineza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    })
});

describe('Bicicleta.add', ()=>{
    it('agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        let a = new Bicicleta(1, 'rojo', 'urbana', [-0.176444, -78.485598]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    })
});

describe('Bicicleta.findById', () => {
    it('Debe devolver la vici con id 1', () => {        
        expect(Bicicleta.allBicis.length).toBe(0);
        let aBici1 = new Bicicleta(1, "verde", "urbana");
        let aBici2 = new Bicicleta(2, "roja", "mountain");

        Bicicleta.add(aBici1);
        Bicicleta.add(aBici2);    

        let targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici1.color);
        expect(targetBici.modelo).toBe(aBici1.modelo);

    })
})

describe('Bicicleta.removeById', () => {
    it('Debe devolver el tamanao del arreglo en 0 despues de eliminar', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        let aBici1 = new Bicicleta(1, "verde", "urbana");
        let aBici2 = new Bicicleta(2, "roja", "mountain");
        Bicicleta.add(aBici1);
        Bicicleta.add(aBici2);    

        Bicicleta.removeById(1);
        expect(Bicicleta.allBicis.length).toBe(1)

    })
}) */


describe('Testing NombreModelo', () => {



    beforeAll( async (done) => {



        await mongoose.connection.close();

        await mongoose.disconnect();



        var mongoDB = 'mongodb://localhost/nombreBaseDeDatos';

        const db = mongoose.connection;

        db.on('error', console.error.bind(console, 'connection error'));

        db.once('open', () => {

            console.log('We are connected to test database!');

        });

        mongoose.set('useFindAndModify', false);

        await mongoose.connect(mongoDB, {

            useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true

        });

        done();

    });


//El siguiente afterEach elimina todos los documentos de cada modelo.

    afterEach( async (done) => {

        try{

            await Usuario.deleteMany({});

            await Bicicleta.deleteMany({});

            await Reserva.deleteMany({});

            done();

        }catch(error){console.error(error)}

    });



    afterAll( async (done) => {

        try{

            await mongoose.connection.close();

            await mongoose.disconnect();

            done();

        }catch(error){console.error(error)}

    });

//Aqui van los tests que desarrollen.

});

