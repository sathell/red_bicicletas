const nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');

let mailConfig;
// ENVIRONMENT HEROKU
if (process.env.NODE_ENV === "production") {
    const options = {
        auth: {
            api_key: process.env.SENDGRID_API_SECRET,
        },
    };
    mailConfig = sgTransport(options);
} else {
    if (process.env.NODE_ENV === "staging") {
      console.log('XXXXXXXXXXXXXXXXXXXXXXXX');
        const options = {
            auth: {
                api_key: process.env.SENDGRID_API_SECRET,
            },
        };
        mailConfig = sgTransport(options);
    } else {
        mailConfig = {
          host: "smtp.mailtrap.io",
          port: 2525,
            auth: {
                user: process.env.MAILTRAP_USER_DEV,
                pass: process.env.MAILTRAP_PASS_DEV
            },
        };
    }
}


/* const mailConfig = {
    host: "smtp.mailtrap.io",
    port: 2525,
    auth: {
        user: "0cdfbe8e1ea67c",
        pass: "94db428e918a9b"
  }
}; */

module.exports = nodemailer.createTransport(mailConfig);