const Bicicleta = require('../models/bicicleta');

/* exports.bicicleta_list = (req, res) => {
    res.render('bicicletas/index', {bicis: Bicicleta.allBicis});
} */

exports.bicicleta_list = function(req, res){
    Bicicleta.allBicis(function(err, bicis) {
        res.render('bicicletas/index', {bicis: bicis});
    });
}


exports.bicicleta_hi = (req, res) => {
    res.send('Hola desde controlador bicis');
}

exports.bicicleta_create_get = (req, res) => {
     res.render('bicicletas/create')
}

exports.bicicleta_create_post = (req, res) => {
    var bici= new Bicicleta(
        {
            code: req.body.code,
            color: req.body.color,
            modelo: req.body.modelo,
            ubicacion: [req.body.lat || 0, req.body.lng || 0]
          } 
          );
          console.log("bici a añadir",bici);
        Bicicleta.add(bici);
        res.redirect('/bicicletas');
}

exports.bicicleta_update_get = (req, res) => {
    
    let bici = Bicicleta.findById(req.params.id);
    
    res.render('bicicletas/update', {bici})
}

exports.bicicleta_update_post = (req, res) => {
    let bici = Bicicleta.findById(req.params.id);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];


    res.redirect('/bicicletas');
}

exports.bicicleta_delete_post = (req, res) => {
    //console.log('parametro del delete '+req.params.id)
    Bicicleta.removeById(req.body.id);

    res.redirect('/bicicletas');
}