const Bicicleta = require('../../models/bicicleta');


exports.bicicleta_list = (req, res) => {
    Bicicleta.allBicis((err, bici)=>{
        //console.log(bici);
        res.status(200).json({
            bicicletas: bici
        })
    })
    
}

exports.bicicleta_create = (req, res) => {
    //console.log(req.body)
    let bici = new Bicicleta({
        code: req.body.id,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.lng]
    });
    
    Bicicleta.add(bici);
    
    res.status(200).json({
        bicicleta: bici
    })
}

exports.bicicleta_delete = function(req, res) {
    //console.log('ENTRO A DELETE!!')
    Bicicleta.removeByCode(req.body.code, ()=>{
        res.status(204).send();
    })
    
}

exports.bicicleta_update = function(req, res) {
    let nuevaBici = {
        code: req.body.id,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.lng]
    };

    let bici = Bicicleta.findByCode({code: req.body.id}, nuevaBici, () => {
        res.status(200).json({
            bicicleta: nuevaBici
        }); 
    });
}